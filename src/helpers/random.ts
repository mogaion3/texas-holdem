export const GetRandomString = (length: number, excludeChars: string[] = []) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
    var charactersLength = characters.length;
    for ( var i = 0; i < length;) {
        var char = characters.charAt(Math.floor(Math.random() * charactersLength));
        if(excludeChars.indexOf(char) === -1){
            result += char;
            i++;
        }
    }
    return result;
}

export const GetRandomStringFrom = (length: number, from: string[]) => {
    var result           = '';
    var characters       = from.join(``);
    var charactersLength = from.length;
    for ( var i = 0; i < length; i++) 
        result += characters.charAt(Math.floor(Math.random() * charactersLength));

    return result;
}

export const GetRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; 
};