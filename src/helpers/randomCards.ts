import { GetRandomInt } from "./random";
import { Cards, Suits } from "./consts";
import { card } from "../models/card";

const GetCard = () => Cards[GetRandomInt(0, Cards.length)];
const GetSuit = () => Suits[GetRandomInt(0, Suits.length)];
let deck: string[];

export const SetDeck = (d: string[]) => {deck = d};

export const GetRandomCardStr = () => {
    let card = `${GetCard()}${GetSuit()}`;
    while(deck.indexOf(card) !== -1)
        card = `${GetCard()}${GetSuit()}`;
    
    deck.push(card);
    return card;
}

export const GetRandomCard = () => {
    return card.create(GetRandomCardStr());
}