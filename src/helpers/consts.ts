export const Suits = ["H", "S", "D", "C"];
export const Cards = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"];

export const BoardCards = 5;
export const PlayerCards = 2;
export const CardsForHandValue = BoardCards + PlayerCards;
export const SuitsCount = Suits.length;
export const CardsCount = Cards.length;