'use strict';

class Combination{
    private size: number;
    private n: number;
    private buffer: number[] = [];
    private combinations = [];

    public get(size: number, n: number) {
        this.combinations = [];
        if(size > n)
            throw new Error(`Cannot create combination of size ${size} from ${n} numbers.`);

        this.size = size;
        this.n = n;

        this.recursiveCombinations();

        return this.combinations;
    }

    private recursiveCombinations(index: number = 0, begin: number = 0){
        for(let i = begin; i < this.n; i++){

            this.buffer[index] = i;

            if(index + 1 < this.size)
                this.recursiveCombinations(index + 1, this.buffer[index] + 1);
            else 
                this.combinations.push(Object.assign([], this.buffer));
            
        }
    }
}

export default new Combination();