'use strict';
import combination from './combination';
import { card } from '../models/card';
import { player } from '../models/player';
import { Cards, Suits, CardsCount, BoardCards, CardsForHandValue } from '../helpers/consts';
import { handValues } from '../enums/handValues';
import { hand } from '../models/hand';
import { suits } from '../enums/suits';

class CardCount{
    constructor(public name: string,  public count: number){}
}
class Texas_holdem{
    private combinations = [];

    public setPlayersHandValue(players: player[], board: card[]){
        this.combinations = combination.get(BoardCards, CardsForHandValue);

        players.forEach(p => this.setHandValue(p, board));
        players.sort((a,b) => this.handValueCmp(a.handValue, b.handValue));
    }

    public displayPlayersInOrer(players: player[]){
        let place = 1;
        for(let i = 0; i < players.length ; i++){
            let playerStr = `${players[i].name} ${handValues[players[i].handValue.value]}`;

            if(players[i].handValue.suit)
                playerStr += ` _${suits[players[i].handValue.suit]}_`;
            if(players[i].handValue.info)
                playerStr += " " + players[i].handValue.info;

            playerStr += " " + players[i].handValue.cards.map(x => x.name).join("");
            console.log(`\t${place}. ${playerStr}`);
            if(i < players.length-1 && players[i].handValue.value === players[i+1].handValue.value && 
                players[i].handValue.score === players[i+1].handValue.score)
                continue;
            else
                place++;
        }
    }

    private setHandValue(p: player, board: card[]){
        let handValues = [];
        let cards = board.concat(p.cards);
        if(cards.length !== CardsForHandValue)
            throw new Error(`Incorrect number of cards. Expected ${CardsForHandValue} but was ${cards.length}.`);

        this.combinations.forEach(combs => {
            let possibleCardComb: card[] = [];
            for (let key of combs)
                possibleCardComb.push(cards[key]);

            handValues.push(this.findHandValues(possibleCardComb));
        });

        p.handValue = this.getBestHandValue(handValues);
    }

    private getBestHandValue(handValues: hand[]): hand{
        handValues.sort((a,b) => this.handValueCmp(a, b));

        return handValues[0];
    }

    private findHandValues(cards: card[]) {
        cards.sort((a,b) => Cards.indexOf(a.name) - Cards.indexOf(b.name))

        let result: hand;
        if(this.hasGaps(cards)){
            if(cards[0].name === Cards[0] && cards[cards.length - 1].name === Cards[CardsCount - 1] && !this.hasGaps(cards.slice(0, -1))){
                let tmp = cards.pop();
                cards.unshift(tmp);
                result = this.checkForFlush(cards);
            }else
                result = this.checkForSameCards(cards);
        }else
            result = this.checkForFlush(cards);

        cards.forEach(c => result.score += Cards.indexOf(c.name));
        result.cards = cards;

        return result;
    }

    private hasGaps(cards: card[]): boolean {
        for(let i = cards.length - 1; i > 0; i--)
            if(Cards.indexOf(cards[i].name) - Cards.indexOf(cards[i-1].name) !== 1)
                return true;
        
        return false;
    }

    private checkForSameCards(cards: card[]){
        let cardsCount = new Array(CardsCount).fill(0);        
        cards.forEach(c => cardsCount[Cards.indexOf(c.name)]++);
        cardsCount = cardsCount.map((value, index) => new CardCount(Cards[index], value));
        cardsCount = cardsCount.filter(x => x.count > 0);

        if(cardsCount.length === BoardCards){
            if(this.hasTheSameSuit(cards))
                return new hand(handValues.Flush, cards[0].suit);

            return hand.createWithInfo(handValues.Highcard, cards[cards.length - 1].name);
        }

        cardsCount.sort((a,b) => b.count - a.count);
        if(cardsCount[0].count === 4)
            return hand.createWithInfo(handValues.Four_of_a_kind, `4x${cardsCount[0].name}`);
        
        if(cardsCount[0].count === 3)
            if(cardsCount[1].count === 2)
                return hand.createWithInfo(handValues.Full_house, `3x${cardsCount[0].name} 2x${cardsCount[1].name}`);
            else
                return hand.createWithInfo(handValues.Three_of_a_kind, `3x${cardsCount[0].name}`);

        if(cardsCount[0].count === 2)
            if(cardsCount[1].count === 2)
                return hand.createWithInfo(handValues.Two_pairs, `2x${cardsCount[0].name} 2x${cardsCount[1].name}`);
            else
                return hand.createWithInfo(handValues.Pair, `2x${cardsCount[0].name}`);
    }

    private checkForFlush(cards: card[]){
        if(this.hasTheSameSuit(cards)){
            if(cards[BoardCards - 1].name === Cards[CardsCount - 1])
                return new hand(handValues.Royal_flush, cards[0].suit);
                
            return new hand(handValues.Straight_flush, cards[0].suit, cards[cards.length - 1].name);
        }

        return new hand(handValues.Straight, null, cards[cards.length - 1].name);
    }

    private hasTheSameSuit(cards: card[]): boolean{
        let suitsCount = new Array(Suits.length).fill(0);

        cards.forEach(c => suitsCount[Suits.indexOf(c.suit)]++);

        return suitsCount.filter(s => s > 0).length === 1;
    }

    private handValueCmp(a: hand, b:hand): number {
        if (a.value === b.value) {
            return b.score - a.score;
        }
        return b.value - a.value;
    }
}

export default new Texas_holdem();