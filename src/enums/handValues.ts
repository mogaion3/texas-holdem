'use strict';
export enum handValues {    
    Highcard,
    Pair,
    Two_pairs,
    Three_of_a_kind,
    Straight,
    Flush,
    Full_house,
    Four_of_a_kind,
    Straight_flush,
    Royal_flush
};
