'use strict';
export enum suits {    
    H = "Hearts",
    S = "Spades",
    D = "Diamonds",
    C = "Clubs"
};
