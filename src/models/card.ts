'use strict';
import { suits } from "../enums/suits";
import { Cards } from "../helpers/consts";

export class card {
    constructor(public name: string,  public suit: string) { }

    public toString = () : string => {
        return this.name + this.suit;
    }

    public static create(input: string){
        if(input.length !== 2)
            throw new Error(`Incorrect card input: ${input}.`);
        if(Cards.indexOf(input[0]) === -1)
            throw new Error(`Unknown card: ${input[0]}.`);
        if(!suits[input[1]])
            throw new Error(`Unknown suit: ${input[1]}.`);

        return new card(input[0], input[1]);
    }
}
