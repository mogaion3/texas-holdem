'use strict';
import { handValues } from "../enums/handValues";
import { card } from "./card";

export class hand {
    public value: handValues
    public suit: string;
    public score: number;
    public cards: card[];
    public info: string;

    constructor(value: handValues, suit: string = null, info = null) {
        this.value = value;
        this.suit = suit;
        this.info = info;
        this.score = 0;
    }

    public static createWithInfo(value: handValues, info: string){
        let obj = new hand(value);
        obj.info = info;
        obj.score = 0;
        return obj;
    }
}
