'use strict';
import { card } from "./card";
import { hand } from "./hand";

export class player {
    public name: string;
    public cards: card[];
    public handValue: hand;

    constructor(name: string){
        this.name = name;
        this.cards = [];
    }

    public static createWithCards(name: string, cards: string[]){
        let obj = new player(name);
        cards.forEach(c => obj.cards.push(card.create(c)));

        return obj;
    }
}
