#!/usr/bin/env node
'use strict';
import readline from 'readline';
import { card } from './models/card';
import { player } from './models/player';
import texas_holdem from './app/texas_holdem';

class App{
    private board: card[] = [];
    private players: player[] = [];
    private args: string[] = [];
    private rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    public init(){
        this.rl.setPrompt("board> ");
        this.rl.prompt();
        
        this.rl.on('line', (input) => {
            this.rl.setPrompt("player> ");
            this.rl.prompt();
            
            if(input === ""){
                this.rl.setPrompt("");
                this.rl.prompt();

                this.parseData();
                texas_holdem.setPlayersHandValue(this.players, this.board);
                
                this.displayResult();

                this.rl.close();
            }else 
                this.args.push(input);
        });
    }

    private parseData(){
        this.args[0].split(" ").forEach(input => this.board.push(card.create(input)));
        for(let i = 1; i < this.args.length; i++){
            let playerInfo = this.args[i].split(" ");
            let playerObj = new player(playerInfo[0]);
            for(let j = 1; j < playerInfo.length; j++)
                playerObj.cards.push(card.create(playerInfo[j]));

            this.players.push(playerObj);
        }
    }

    private displayResult(){
        console.log("\n\tOutput:");
        texas_holdem.displayPlayersInOrer(this.players);
    }
}

new App().init();
