import texas_holdem from '../src/app/texas_holdem';
import { assert } from 'chai';
import { player } from '../src/models/player';
import { card } from '../src/models/card';
import { suits } from '../src/enums/suits';
import { handValues } from '../src/enums/handValues';
import { CardsForHandValue, PlayerCards } from '../src/helpers/consts';
import { SetDeck, GetRandomCard } from '../src/helpers/randomCards';

describe('texas_holdem.setPlayersHandValue', () => {
    SetDeck([]);
    const data = [3, 8];
    let players: player[] = [];
    for(let i = 0; i < 3; i++){
        let p = new player("john Smith");
        p.cards = new Array(PlayerCards).fill("").map(x => GetRandomCard());
        players.push(p);
    }
    data.forEach(d => {
        it(`should throw error when board size is wrong (${d})`, () => {
            let expectedErrorMsgPattern = new RegExp(`^Incorrect number of cards. Expected ${CardsForHandValue} but was ${d + 2}.$`);
            let board = new Array(d).fill("").map(x => GetRandomCard());
            
            assert.throws(() => texas_holdem.setPlayersHandValue(players, board), Error, expectedErrorMsgPattern);
        });
    });

    it(`should return players in order`, () => {
        let expectedPlayers = [
            {player: 'Becky', handValue: 'Straight A'},
            {player: 'Sam', handValue: 'Two_pairs 2xK 2xA'},
            {player: 'John', handValue: 'Pair 2x7'}
        ]    
        let board = [
            card.create('KS'),
            card.create('AD'),
            card.create('3H'),
            card.create('7C'),
            card.create('TD')
        ];
        let pls = [
            player.createWithCards("John", ['9H', '7S']),
            player.createWithCards("Sam", ['AC', 'KH']),
            player.createWithCards("Becky", ['JD', 'QC']),
        ];
        
        texas_holdem.setPlayersHandValue(pls, board);
        
        const result = pls.reduce((accumulator, pl, index) => {
            accumulator[index] = {player: pl.name, handValue: handValues[pl.handValue.value] + " " + pl.handValue.info};
            return accumulator;
          }, []);

        assert.sameDeepMembers(result, expectedPlayers);
    });

    it(`should find straight flush starting with A`, () => {
        let expectedPlayers = [
            {player: 'Becky', handValue: 'Straight_flush _Clubs_ 5'},
            {player: 'Sam', handValue: 'Pair 2xK'},
            {player: 'John', handValue: 'Highcard A'},
            {player: 'Ion', handValue: 'Highcard A'}
        ]    
        let board = [
            card.create('KS'),
            card.create('9D'),
            card.create('2C'),
            card.create('AC'),
            card.create('3C')
        ];
        let pls = [
            player.createWithCards("John", ['6D', 'TD']),
            player.createWithCards("Sam", ['JD', 'KH']),
            player.createWithCards("Becky", ['4C', '5C']),
            player.createWithCards("Ion", ['6C', '7S'])
        ];
        
        texas_holdem.setPlayersHandValue(pls, board);
        
        const result = pls.reduce((accumulator, pl, index) => {
            accumulator[index] = {};
            accumulator[index].player = pl.name;
            accumulator[index].handValue = handValues[pl.handValue.value] + " ";

            if(pl.handValue.suit)
                accumulator[index].handValue += `_${suits[pl.handValue.suit]}_ `;
            
            accumulator[index].handValue += pl.handValue.info;
            return accumulator;
          }, []);

        assert.sameDeepMembers(result, expectedPlayers);
    });
});