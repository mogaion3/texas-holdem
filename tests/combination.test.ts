import combination from '../src/app/combination';
import { assert } from 'chai';

describe('combination.get', () => {    
    it('should throw error when size is greather than numbers count', () => {
        const size = 4;
        const n = 3;
        let expectedErrorMsgPattern = new RegExp(`^Cannot create combination of size ${size} from ${n} numbers.$`);
        
        assert.throws(() => combination.get(size, n), Error, expectedErrorMsgPattern);
    });
    
    it('should return expected combinations', () => {
        let expected = [ [ 0, 1 ], [ 0, 2 ], [ 1, 2 ] ];
        let result = combination.get(2, 3);
        
        assert.sameDeepMembers(result, expected);
    });
  });