import { BoardCards, PlayerCards } from "../src/helpers/consts";
import { GetRandomCard, SetDeck } from "../src/helpers/randomCards";
import { player } from "../src/models/player";
import { GetRandomInt } from "../src/helpers/random";
import texas_holdem from "../src/app/texas_holdem";

describe('texas_holdem.try', () => {
    SetDeck([]);
    it(`should display result`, () => {
        console.log(`\nInput:`);
        let board = new Array(BoardCards).fill("").map(x => GetRandomCard());
        console.log(`\tboard> ${board.join(" ")}`);

        let pls: player[] = [];
        for(let i = 0; i < GetRandomInt(2, 11); i++){
            let p = new player(`Player_${i+1}`);
            p.cards = new Array(PlayerCards).fill("").map(x => GetRandomCard());
            pls.push(p);
            console.log(`\tplayer> ${p.name} ${p.cards.join(" ")}`);
        }

        console.log(`\nOutput:`);
        texas_holdem.setPlayersHandValue(pls, board);
        texas_holdem.displayPlayersInOrer(pls);
    });
});