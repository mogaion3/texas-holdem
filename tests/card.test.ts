import { card } from '../src/models/card';
import { GetRandomString, GetRandomInt } from '../src/helpers/random';
import { assert } from 'chai';
import { Suits, Cards } from '../src/helpers/consts';

describe('card.create', () => {
    const data = [1, 3];
    const GetRandomCard = () => Cards[GetRandomInt(0, Cards.length)];
    const GetRandomSuit = () => Suits[GetRandomInt(0, Suits.length)];

    data.forEach(d => {  
        it(`should throw error when card input length is not accepted(${d})`, () => {
            let cardInput = GetRandomString(d);
            let expectedErrorMsgPattern = new RegExp(`^Incorrect card input: ${cardInput}.$`);
            
            assert.throws(() => card.create(cardInput), Error, expectedErrorMsgPattern);
        });
    }); 
    it(`should throw error when card is unknown`, () => {
        let cardInput = GetRandomString(2, Cards);
        let expectedErrorMsgPattern = new RegExp(`^Unknown card: ${cardInput[0]}.$`);
        
        assert.throws(() => card.create(cardInput), Error, expectedErrorMsgPattern);
    });
    it(`should throw error when suit is unknown`, () => {
        let cardInput = `${GetRandomCard()}${GetRandomString(1, Suits)}`;
        let expectedErrorMsgPattern = new RegExp(`^Unknown suit: ${cardInput[1]}.$`);
        
        assert.throws(() => card.create(cardInput), Error, expectedErrorMsgPattern);
    });
    it(`should return expected card object`, () => {
        let cardInput = `${GetRandomCard()}${GetRandomSuit()}`;
        let cardObj = card.create(cardInput);

        assert.equal(cardObj.name, cardInput[0]);
        assert.equal(cardObj.suit, cardInput[1]);
    });
});