# Texas Holdem
------
This project was written in nodeJS. It is a Console application and also contains unit tests.

## Condition
Write a program that can identify and rank [Texas Hold'em](https://en.wikipedia.org/wiki/Texas_hold_'em) poker hands.
The results should be output to stdout. Each line should contain the rank, the player's name, and their hand name. The hand name should have a full description including kickers if necessary.

#### Specifications
Your program should read the community cards and each player's hand from stdin. The first line will contain the five community cards. Each line after that will have a player's name followed by their two cards. Cards will be identified using two characters. First will be the face, followed by the suit. The characters used to represent the cards include the digits 2 through 9 representing their respective values. For other characters:  

| Character | Represents |
|-----------|------------|
| T         | 10         |
| J         | Jack       |
| Q         | Queen      |
| K         | King       |
| A         | Ace        |
| H         | Hearts     |
| S         | Spades     |
| D         | Diamonds   |
| C         | Clubs      |

#### Example Input:
```
KS AD 3H 7C TD
John 9H 7S
Sam AC KH
Becky JD QC
```

#### Example Output:
```
1 Becky Straight Ace
2 Sam Two Pair Ace King
3 John Pair 7
```

## Getting Started
------
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#### Installing
1. Clone project on your local PC:  
	```
	git clone https://mogaion3@bitbucket.org/mogaion3/texas-holdem.git
	```
2. Open the folder texas-holdem and run:  
	```
	npm i && npm run build
	```
3. In the same folde run:  
	```
	npm run start
	```  
	*Hint:* Don't know input data?! Run: ```npm run try```


#### Testing
1. Open the folder texas-holdem and run:  
	```
	npm t
	```

------
#### BONUS
------
Want to make app available globally in an unix environment?!  
Run: ``` sudo npm link ``` and enjoy the app by calling ``` texas-holdem-agom ``` .